# OpenML dataset: Gambling-Behavior-Bustabit

https://www.openml.org/d/43364

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The similarities and differences in the behaviors of different people have long been of interest, particularly in psychology and other social science fields. Understanding human behavior in particular contexts can help us to make informed decisions. Consider a game of poker - understanding why players raise, call, and fold in various situations can provide a distinct advantage competitively.
Along these lines, we are going to focus on the behavior on online gamblers from a platform called Bustabit. There are a few basic rules for playing a game of Bustabit:
You bet a certain amount of money (in Bits, which is 1 / 1,000,000th of a Bitcoin) and you win if you cash out before the game busts.
Your win is calculated by the multiplier value at the moment you cashed out. For example, if you bet 100 and the value was 2.50x at the time you cashed out, you win 250. In addition, a percentage Bonus per game is multiplied with your bet and summed to give your final Profit in a winning game. Assuming a Bonus of 1, your Profit for this round would be (100 x 2.5) + (100 x .01) - 100 = 151

The multiplier increases as time goes on, but if you wait too long to cash out, you may bust and lose your money.
Lastly, the house maintains slight advantages because in 1 out of every 100 games, everyone playing busts.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43364) of an [OpenML dataset](https://www.openml.org/d/43364). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43364/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43364/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43364/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

